/**
 * Calculator.java
 * Copyright (c) Branden Ogata 2016
 */
package edu.hawaii.ics211;

/**
 * Calculator, a simple calculator.
 * 
 * @author Branden Ogata
 * 
 */
public class Calculator {
  /**
   * Returns the sum of two integers.
   * 
   * @param first The first int to add.
   * @param second The second int to add.
   * 
   * @return An int equal to the sum of the parameters.
   * 
   */

  public int add(int first, int second) {
    return first + second;
  }


  /**
   * Returns the difference between two integers.
   * 
   * @param first The int to subtract from.
   * @param second The int to subtract.
   * 
   * @return An int equal to the difference of the parameters.
   * 
   */

  public int subtract(int first, int second) {
    return first - second;
  }


  /**
   * Returns the product of two integers.
   * 
   * @param first The first int to multiply.
   * @param second The second int to multiply.
   * 
   * @return An int equal to the product of the parameters.
   * 
   */

  public int multiply(int first, int second) {
    return first * second;
  }


  /**
   * Returns the quotient of two integers.
   * 
   * @param first The int containing the value of the numerator.
   * @param second The int containing the vlaue of the denominator.
   * 
   * @return An int equal to the quotient of the parameters.
   * 
   */

  public int divide(int first, int second) {
    return first / second;
  }


  /**
   * Returns the modulus of two integers.
   * 
   * @param first The int containing the value of the numerator.
   * @param second The int containing the value of the denominator.
   * 
   * @return An int equal to the remainder from the division of the parameters.
   * 
   */

  public int modulo(int first, int second) {
    return first % second;
  }


  /**
   * Returns the value of the first integer to the power of the second integer.
   * 
   * @param first The int containing the value of the base.
   * @param second The int containing the value of the exponent.
   * 
   * @return An int equal to the value of the first argument to the power of the second integer.
   * 
   */

  public int pow(int first, int second) {
    int total = 1;

    for (int i = 0; i < second; i++) {
      total *= first;
    }

    return total;
  }
}
