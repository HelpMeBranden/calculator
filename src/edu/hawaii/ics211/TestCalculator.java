package edu.hawaii.ics211;
/**
 * TestCalculator
 * @author Steven Braun
 * Tester class that tests all of the methods from the Calculator class to make sure they work properly
 */
public class TestCalculator {
	public static void main(String[] args) {

		Calculator calc = new Calculator();

		System.out.println(calc.add(3, 24));
		System.out.println(calc.add(0, 24));
		System.out.println(calc.add(3, -24));
		System.out.println(calc.divide(4, 8));
		System.out.println(calc.divide(8, 4));
		System.out.println(calc.divide(3, 1));
		System.out.println(calc.divide(3, -3));
		System.out.println(calc.divide(-3, -3));
		System.out.println(calc.modulo(4, 3));
		System.out.println(calc.multiply(0, 13));
		System.out.println(calc.multiply(13, -13));
		System.out.println(calc.multiply(-13, -13));
		System.out.println(calc.pow(3, 3));
		System.out.println(calc.pow(3, -3));
		System.out.println(calc.pow(-3, 2));
		System.out.println(calc.pow(-443, 0));
		System.out.println(calc.subtract(5, 0));
		System.out.println(calc.subtract(0, 24));
		System.out.println(calc.subtract(-3, -2));
	}
}
